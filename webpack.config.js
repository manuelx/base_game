const path = require('path');

module.exports = {
    entry: './src/index.ts',
    devtool: 'inline-source-map',
    devServer: {
      // Display only errors to reduce the amount of output.
      stats: "errors-only",
  
      // Parse host and port from env to allow customization.
      //
      // If you use Docker, Vagrant or Cloud9, set
      // host: "0.0.0.0";
      //
      // 0.0.0.0 is available to all network devices
      // unlike default `localhost`.
      host: "192.168.0.17", // Defaults to `localhost`
      port: 3000, // Defaults to 8080
      open: true, // Open the page in browser
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/
        }
      ]
    },
    resolve: {
      extensions: [ '.tsx', '.ts', '.js' ],
      alias: {
        '@Utils': path.resolve(__dirname, 'src/Utils/'),
        '@Config': path.resolve(__dirname, 'src/Config/'),
        '@Game': path.resolve(__dirname, 'src/Game/'),
        '@App': path.resolve(__dirname, 'src/App/')
      }
    },
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist')
    },
    externals: {
      'pixi.js': 'PIXI',
      'lodash' : '_',
      'Signals': 'signals'
    }

};