import App from '@App/App';

const _appConfig = {
    width: 800, 
    height: 600, 
    backgroundColor: 0x1099bb, 
    resolution: 1/*window.devicePixelRatio || 1,*/
}

const app = new App( _appConfig );

document.body.appendChild(app.view);