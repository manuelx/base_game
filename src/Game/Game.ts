import * as PIXI from 'pixi.js';
import {testFunction} from '@Utils/Utils'
import AssetManager from '@App/AssetManager';
//import '@Utils/pixi-spine';

export default class Game extends PIXI.Container{
    
    private _elements: PIXI.Container[];

    constructor(){
        super();
        this._elements  = [];

        var _ufoAsset:PIXI.Sprite = AssetManager.assetManager.getAsset("ufo");
        this.addChild(_ufoAsset);
        _ufoAsset.alpha = 0.5;

        var wild:PIXI.spine.Spine = AssetManager.assetManager.getAsset("wild");
        
        // add the animation to the scene and render...
        this.addChild(wild);
        wild.height = 500;
        wild.scale.x = wild.scale.y;

        if (wild.state.hasAnimation('extending')) {
            // run forever, little boy!
            wild.state.setAnimation(0, 'extending', true);
            // dont run too fast
            //wild.state.timeScale = 0.1;
        }

        testFunction();
    }
}