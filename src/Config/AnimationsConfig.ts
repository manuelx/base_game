export const animationsConfig = {
    avatarPirate : {
        name      : 'pirate',
        animations: [
            {
                name              : 'jump_in',
                loop              : true,
                delay             : 0           //second   
            },
            {
                name              : 'idle',
                loop              : true,
                delay             : 0           //second   
            },
            {
                name              : 'keep_gun',
                loop              : true,
                delay             : 1           //second       
            },
            {
                name              : 'recieve_bullet',
                loop              : true,
                delay             : 0           //second       
            },
            {
                name              : 'win',
                loop              : true,
                delay             : 0           //second       
            },
            {
                name              : 'go_away',
                loop              : false,
                delay             : 2           //second       
            }
        ],
        mixAnimations:[
            {
                from : 'win',
                to   : 'go_away',
                mixTime: 1 //second
            }
        ],
        queueAnimations: [
            {
                nameQueue  : 'Pay',
                queueList  : [ 
                    'jump_in',
                    'idle',
                    'keep_gun',
                    'recieve_bullet',
                    'win',
                    'go_away'
                ]
            },
            {
                nameQueue  : 'disparo',
                queueList  : [ 
                    'jump_in',
                    'idle',
                    'keep_gun',
                ]
            }
        ]
    },

    avatarPirateDisparo : {
        name              : 'recieve_bullet',
        loop              : false,
        delay             : 0 
    },

    avatarPirateIdle: {
        name              : 'idle',
        loop              : true,
        delay             : 0
    }
}
