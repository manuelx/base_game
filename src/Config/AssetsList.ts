export const modules = [
   // "intro",    
    "main"   
];

export const assetsList = {
   /* "intro":[
        { "id": "languageDictionary", "src": "xml/Language_Kraken_EN.xml", "type": "xml", "priority": "1" }
    ],*/

    "main":[
        { "id": "ufo", "src": "main/c_ufo.png", "type": "image", "priority": "" },
        { "id": "wild", "src": "spine/extended_wild.json", "type": "jsonSpine", "priority": "" }
    ]
}
