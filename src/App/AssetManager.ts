import * as PIXI from 'pixi.js';
import * as _ from 'lodash';
import {gameConfig} from '@Config/GameConfig';
import {modules, assetsList} from '@Config/AssetsList';
import {Signal} from 'Signals';


export default class AssetManager {
    static assetManager:AssetManager;

    // Se despacha cada vez que se completa la carga de un modulo.
    onModuleLoaded:Signal; 

	_staticAssetsPool   = [];
    _animatedAssetsPool = [];
	_loader:PIXI.loaders.Loader;
	_loadingModuleManifestIndex:any = 0;
	_totalModulesManifest:any       = 0;
	_modulesManifest:any            = [];
	_priorityCounter:any            = {};
	_assetList:any                  = {
		"image": {},
		"music": {},
		"sfx"  : {},
		"misc" : {}
	};

	_typeMap:any = {
		"image": {
            "load": 2,
            "xhr" : "text"
        },
        "svg": {
            "load": 1,
            "xhr" : "document"
        },
        "sound": {
            "load": 3,
            "xhr" : "text"
        },
        "xml": {
            "load": 1,
            "xhr" : "document"
        },
        "spriteSheet": {
            "load": 1,
            "xhr" : "json"
        },
        "json": {
            "load": 1,
            "xhr" : "json"
        }
    };

    /** INICIO - MODULO DE CARGA DE ASSETS */
    
    constructor(){
        this._loader = new PIXI.loaders.Loader();
        this.onModuleLoaded = new Signal();
    }

	_loadManifest (url:any, completeHandler:any) {
		var moduleName = url.substring(url.indexOf("mod=")+4, url.indexOf("&", url.indexOf("mod=")));
		
		if (!this._loader) this._loader = new PIXI.loaders.Loader();
		
		this._loader.reset();

		this._loader.add(url, {metadata: {module: moduleName}, xhrType: "json"}).load(function(loader, resources){
			var jsonManifest = resources[url].data;
			var moduleName = resources[url].metadata.module;
			completeHandler && completeHandler(jsonManifest, moduleName);
		});
	};

	_onMainManifestLoaded (jsonManifest:any, moduleName:any){
        var that = this;
        // Al manifest principal le hago modificaciones para los sprites y la escala
		var _manifestToLoad:any = {};
		var _manifest 		= null;

		this._modulesManifest      = modules;
		this._totalModulesManifest = this._modulesManifest.length;

		_manifestToLoad.path 	= jsonManifest.path;
		_manifest 				= this._setManifestFileScale(jsonManifest, "");
		//_manifest 				= this._expandManifestSprite(_manifest);

		// Reinicio el loader con 15 conexiones activas
		this._loader = new PIXI.loaders.Loader("", 15);

		_.each(_manifest, function(e){
			if (!e.hasOwnProperty("priority")) return;
			if (e.priority == ""){
				e.priority = "z"; // Tengo que hacer esta boludez, sino el sort me pone primero los de priority == "".
			} 
			if (!that._priorityCounter.hasOwnProperty(e.priority)) that._priorityCounter[e.priority] = 0;
			that._priorityCounter[e.priority]++;
		});

		_manifest 				= _.sortBy(_manifest, "priority");
		var _manifestByPriority = _.groupBy(_manifest, "priority");
		var _priorities 		= Object.keys(_manifestByPriority);
		that._loadAssetsByPriority(_manifestToLoad, _manifestByPriority, moduleName, _priorities);
	};


	_onBonusManifestLoaded (jsonManifest: any, moduleName:any){
        var that = this;
        var _manifestToLoad:any 	= {};
		var _manifest:any 			= null;

		_manifestToLoad.path 	= jsonManifest.path;
		_manifest 				= this._setManifestFileScale(jsonManifest, "");
	//	_manifest 				= this._expandManifestSprite(_manifest);

		// Reinicio el loader con 1 conexión activa
		this._loader = new PIXI.loaders.Loader("", 1);

		_.each(_manifest, function(e){
			if (!e.hasOwnProperty("priority")) return;
			if (e.priority == ""){
				e.priority = "z"; // Tengo que hacer esta boludez, sino el sort me pone primero los de priority == "".
			} 
			if (!that._priorityCounter.hasOwnProperty(e.priority)) that._priorityCounter[e.priority] = 0;
			that._priorityCounter[e.priority]++;
			
		});

		_manifest = _.sortBy(_manifest, "priority");
		var _manifestByPriority = _.groupBy(_manifest, "priority");
		var _priorities 		= Object.keys(_manifestByPriority);
		that._loadAssetsByPriority(_manifestToLoad, _manifestByPriority, moduleName, _priorities);
		
	};

	_loadAssetsByPriority (manifestToLoad:any, manifestByPriority:any, moduleName:any, priorities:any){
        var that = this;
        var _currentPriority 	= priorities.shift();
		manifestToLoad.manifest = manifestByPriority[_currentPriority]
			
		that._loadAssets(manifestToLoad, moduleName, function(e:any){ that._onMainAssetsProgress(e); }, function(resources:any){ 
			that._onAssetsComplete(resources); 
			
			// Si no termine de cargar las prioridades, cargo la siguiente (de manera recursiva).
			if (priorities.length){
				//pubsub.publicar(LeanderMob.NotifyConst.PRIORITY_LIST_COMPLETED, _currentPriority);
				that._loadAssetsByPriority(manifestToLoad, manifestByPriority, moduleName, priorities);
			}else{
				that._onManifestCompleted(moduleName);
			}
		});
	};

	_loadAssets (manifestToLoad:any, moduleName:any, progressHandler:any, completeHandler:any){
        var that = this;
        var itemsTotal:any = manifestToLoad.manifest.length;
		var itemsLoaded:any = 0;
		var totalSounds:any = 0;
		var soundList:any = {};
		
		if (this._loader) this._loader.reset();

		for (var i = 0; i < manifestToLoad.manifest.length; i++){
			var asset = manifestToLoad.manifest[i];
			var type = this._typeMap[asset.type];
			// Los archivos de audio los maneja SoundJS directamente
			if (asset.type == "sound"){
				if (!soundList[asset.src]) soundList[asset.src] = {id: []};
				soundList[asset.src].id.push(asset.id);
				soundList[asset.src].metadata = {priority : (!isNaN(parseInt(asset.priority)) ? asset.priority : "")};
				totalSounds++;
			} else if(asset.type == "jsonSpine"){
				if(this._loader)this._loader.add(asset.id, asset.src);
			}else {
				if(this._loader)this._loader.add(asset.id, asset.src, {metadata: {module: moduleName, type: asset.type, priority: (!isNaN(parseInt(asset.priority)) ? asset.priority : "")}, loadType: type.load, xhrType: type.xhr});
			}
		}

		// Levanto todos los srcs de audio (sin importar los ids, quiero los elementos), después asigno bien todo.
		var uniqSoundSrcList = _.map(Object.keys(soundList), function(elem){ return {src: elem}; });
		if (uniqSoundSrcList.length > 0){
			/*createjs.Sound.on("fileerror", function(e){
				showSoundProgress(e);
			});
			createjs.Sound.on("fileload", function(e){
				showSoundProgress(e);
			});
            createjs.Sound.registerSounds(uniqSoundSrcList);
            */
		} else {
			loadRemainingAssets();
		}

		function showSoundProgress(e:any){
			// Guardo la instancia en cada uno de los items que tengan ese src (puede haber más de uno)
			if (!soundList[e.src]){
				soundList[e.src] = {};
			}

			soundList[e.src].data = e.target;
			itemsLoaded++;

			var percSoundsLoaded = (itemsLoaded / uniqSoundSrcList.length) * totalSounds;
			var percTotalLoaded = percSoundsLoaded / itemsTotal;
			progressHandler && progressHandler({item: soundList[e.src], loaded: itemsLoaded, total: itemsTotal, progress: percTotalLoaded});
			
			if (itemsLoaded == uniqSoundSrcList.length){
				parseAllSounds();
			}
		}

		function parseAllSounds(){
			// Ya están todos los audios levantados, ahora creo todos los Resources necesarios
			for (var src in soundList){
				var elem = soundList[src];
				var data = elem.data;
				for (var i = 0; i < elem.id.length; i++){
					var elemId = elem.id[i];
					var elemSrc = src;
					var asset:any = new PIXI.loaders.Resource(elemId, "");
					asset.complete();
					asset.type = 4;//PIXI.loaders.Resource.TYPE.AUDIO;
					asset.data = data;
					asset.metadata = {module: moduleName};
					asset.url= elemSrc;

					if (elemId.indexOf("music_") >= 0){
						that._assetList.music[elemId] = asset;
					} else if (elemId.indexOf("sfx_") >= 0){
						that._assetList.sfx[elemId] = asset;
					}
				}
			}

			//createjs.Sound.removeAllEventListeners("fileload");

			loadRemainingAssets();
		}

		function loadRemainingAssets(){
			// Comienzo la carga del resto de los assets
			if (that._loader) that._loader.on("progress", function(e, r){
				itemsLoaded++;
				/*progressHandler && progressHandler({item: r, loaded: itemsLoaded, total: itemsTotal, progress: (itemsLoaded / itemsTotal)});*/
				progressHandler && progressHandler({item: r, loaded: itemsLoaded, total: itemsTotal, progress: e.progress / 100 });
			});

			that._loader.load(function(loader, resources){
				that._loader.removeAllListeners("progress");
				completeHandler && completeHandler(resources);
			});
		}
	};

	_onMainAssetsProgress (e:any){
		//console.log("_onMainAssetsProgress " +e.progress +" / "+e.loaded+" / "+e.total);
		//gmApi.setLoadProgress(parseInt(e.progress * 100));
	};

	_onBonusAssetsProgress (e:any){
        
        var priority = (e.item.metadata && e.item.metadata.priority) ? e.item.metadata.priority : false;
		if (priority != undefined && this._priorityCounter.hasOwnProperty(priority)){
			this._priorityCounter[priority]--;
			if (this._priorityCounter[priority] <= 0){
				//pubsub.publicar(LeanderMob.NotifyConst.PRIORITY_LIST_COMPLETED, priority);
			}
		}
		//gmApi.setLoadProgressExtraModules(parseInt(e.progress * 100));
	};

	_onAssetsComplete (resources:any){
        var that = this;
        var moduleName = ""; // Lo voy a necesitar más adelante
		var multipackedRes:any = {}; // Voy a guardar todos los que necesiten un parseo posterior

		//gmApi.setLoadProgress(100);

		// Hago un post-proceso de todos los resources para separarlos en categorías y parsear los que así lo requieran
		_.each(resources, function(elem){
			if (moduleName == "") moduleName = elem.metadata.module; // Todos tienen el mismo, me lo guardo solo la primera vez

			// Primero los separo por categorías
			switch(elem.metadata.type){
				case "image":
				case "svg":
                    that._assetList.image[elem.name] = elem;
					break;
				case "sound":
					// Lo ignoro porque ya lo agregó al assetList antes
					break;
				case "xml":
				case "spriteSheet":
				case "json":
				default:
					// Reviso si es un image que vino de un spriteSheet
					var hasSuffix = (elem.name.substr(elem.name.length - 6) == "_image");
					if (hasSuffix){
						var realName = elem.name.replace("_image", "");
						var multipackedName = elem.name.substr(0, elem.name.lastIndexOf("-")); // me fijo si era parte de un multipacked
						if ( (that._assetList.misc[realName] == null) && (multipackedName.length == 0) ){
							that._assetList.misc[elem.name] = elem;
						}
					} else {
						that._assetList.misc[elem.name] = elem;
					}
					break;
			}

			// Ahora parseo los que sean atlas o spriteSheets
			if (elem.hasOwnProperty("textures")){
				var textures = elem.textures;
				var ret:any = {};

				//verificamos si existe la propiedad meta y si su valor es movieClip, para que el sprite se recorte con los valores del JSON
				//y no con los datos de la URL c=x; r=x; f=x
				if ( elem.data.hasOwnProperty("meta") && elem.data.meta.hasOwnProperty("type") && elem.data.meta.type == "movieClip" ){

					// Verifico si es parte de un multipack o no
					if (elem.data.meta.hasOwnProperty("multipacked") && elem.data.meta.multipacked == "true"){
						// Uno sus texturas a un nuevo item con el id unificado
						var unifiedId = elem.data.meta.id.substr(0, elem.data.meta.id.lastIndexOf("-"));

						elem.textures = _.map(elem.textures, function(e, k){
							var newKey = parseInt(k.substr(k.lastIndexOf("_") + 1)); 
							return {id: newKey, texture: e};
						});

						if (!resources[unifiedId]){
							multipackedRes[unifiedId] = elem;
							resources[unifiedId] = elem;
							that._assetList.misc[unifiedId] = elem;
						} else {
							resources[unifiedId].textures = resources[unifiedId].textures.concat(elem.textures);
						}
						// Una vez agregado al unificado, ya no necesito su referencia
						delete that._assetList.misc[elem.name];
						that._assetList.misc[unifiedId].name = unifiedId;
					} else {
						elem.textures	= _.values(elem.textures);
						elem.texture 	= elem.textures[0];
					}

				}else{

					_.each(textures, function(tex, id, list){
						ret[id] = new PIXI.loaders.Resource(id, "");
						ret[id].complete();
						ret[id].type = 3;//PIXI.loaders.Resource.TYPE.IMAGE;
						ret[id].data = tex.baseTexture.source;
						ret[id].metadata = {module: moduleName};
						ret[id].texture = tex;
						//if(LeanderMob.GameConfig.USE_IDLE_SYMBOLS) { ret[id].textures = [tex]; }
						//ret[id].url= LeanderMob.GameConfig.ASSETS_FOLDER + "/" + gmApi.getLayoutResolutionAssets() + "/" + moduleName + "/" + id + ".png";
					});

					that._assetList.image = _.extend(that._assetList.image, ret);
				}
				
			}

			// Y ahora los que sean movieclips
			//verificamos si existe la propiedad meta y si su valor es movieClip	
			/*if ((elem.type == 2) && elem.url.indexOf("sprite") >= 0){
				var imgInfo 		= LeanderMob.Utils.getSpriteData(elem.url);
				var baseTexture 	= elem.texture.baseTexture;
				var textures 		= [];

				// Cropeo de las texturas para los MovieClips
				for (var row = 0; row < parseInt(imgInfo.rows); row++){
					for (var col = 0; col < parseInt(imgInfo.cols); col++){
						var currIndex = (row * parseInt(imgInfo.cols)) + col;
						if (currIndex >= parseInt(imgInfo.frames)) break;
						var tHeight = baseTexture.realHeight/imgInfo.rows;
						var tWidth = baseTexture.realWidth/imgInfo.cols;
						var cropRectangle = new PIXI.Rectangle((tWidth*col), (tHeight*row) ,tWidth,tHeight);
						textures.push(new PIXI.Texture(baseTexture,cropRectangle, cropRectangle))
					}
				}
				elem.textures 	= textures;
				elem.texture 	= textures[0];
			}*/
		});

		// Parseo posterior de todos los multipacked. Tengo que asegurarme de que estén correctamente en orden.
		for (var id in multipackedRes){
			var elem = multipackedRes[id];
			var textures = _.sortBy(elem.textures, "id");
			elem.textures = _.map(textures, function(e){ return e.texture; });
		}
	};

	_onManifestCompleted (moduleName:any){
		// Notifico la finalización de la carga principal (si era el "main")
		/*if ( !gmApi.getMainLoadComplete() ) {
			gmApi.setMainLoadComplete();
		}*/

		// esta publicacion es escuchada por appModel, quien se subscribe cuando tiene q lanzar un modulo
		// que no tiene los assets cargados.
		//pubsub.publicar(LeanderMob.NotifyConst.MANIFEST_FILES_COMPLETE, moduleName);

        this.onModuleLoaded.dispatch(moduleName);

		// Comienzo (o avanzo al siguiente) la carga de los módulos de bonus
		this._loadNextBonus();
	};

	_loadNextBonus (){
        var that = this;
        this._loadingModuleManifestIndex++;
		// Cargo el manifest del próximo módulo de bonus (si hubiere)
		if (this._loadingModuleManifestIndex < this._totalModulesManifest){
			var jsonFile = this._modulesManifest[this._loadingModuleManifestIndex];
			/*this._loadManifest(jsonFile, function(manifest:any, moduleName:any){
            });*/
            that._onBonusManifestLoaded(assetsList, modules[this._loadingModuleManifestIndex]);
			
		}
	}

	_getItem(name:any){
		var ret = null;

		for (var type in this._assetList){
			if (this._assetList[type][name]){
				ret = this._assetList[type][name];
				break;
			}
		}

		return ret;
	}

	// Auxiliares
	_setManifestFileScale (manifest:any, path:any) {

		// Get Scale
		//var layoutVars = gmApi.getLayoutVars();
		//var escala = 1;//layoutVars.scale;
		//var cacheMark = layoutVars.cacheMark;
        var resolutionAssets = gameConfig.assetsResolution; // gmApi.getLayoutResolutionAssets();
        var assetsFolder = gameConfig.assetsFolder;
		//var isIOS = LeanderMob.Utils.Environment.isIOS();

		for (var i = 0; i < manifest.length; i++) {		
			
			if ((manifest[i].type.toLowerCase() == "image") || 
				(manifest[i].type.toLowerCase() == "svg") || 
				(manifest[i].type.toLowerCase() == "jsonspine") || 
				(manifest[i].type.toLowerCase() == "spritesheet")){

					if (path == "" || !path) {
						manifest[i].src = assetsFolder + "/" + resolutionAssets + "/" + manifest[i].src;
					}

			}
		}

		return manifest;
	};

	_expandManifestSprite (manifest:any) {
		var itemsToAdd:any = [];
		var i, j, len;

		len = manifest.length;

		for (i = 0; i < len; i++) {
			var currentItem = manifest[i];
			/*if (currentItem.src.indexOf(LeanderMob.FileTypeConst.SPRITE) != -1) {
				var item_src= currentItem.src;
				var item_id = currentItem.id;
				var indexOf_f = item_src.indexOf("_f=");
				var indexOf_i = item_src.indexOf("_i=");
				// if f is not found, add normal sprite
				if (indexOf_f >= 0 && indexOf_i >= 0){
					var totalFrames = parseInt(item_src.substr(indexOf_f + 3, item_src.indexOf("_", indexOf_f + 1)));

					manifest.splice(i, 1);
					i--;
					len--;

					for (j = 0; j < totalFrames; j++){
						itemsToAdd.push({id: item_id + "_" + j, src: item_src.replace("%", j), type: "image"});
					}
				}
			}*/
		}

		for (i = 0; i < itemsToAdd.length; i++){
			manifest.push(itemsToAdd[i]);
		}

		return manifest;
    };
    
	start(){
        // Primero cargo el manifest principal
       /* var that = this;
        var jsonFile = "php/assets_json_generator.php";
        jsonFile += "?mod=main";
        jsonFile += "&locale=" + gmApi.getLocale();
        jsonFile += "&devType=" + (LeanderMob.Utils.Environment.isMobile() ? "MOBILE" : "DESKTOP");

        this._loadManifest(jsonFile, function(manifest, moduleName){
            that._onMainManifestLoaded(manifest, moduleName);
        });*/
        this._onMainManifestLoaded(assetsList.main, "main");
    }

    /*uploadAssetsToGPU (){
        _.each(this._assetList.image, function(elem){
            if (elem.textures){
                for (var i = elem.textures.length-1; i > 0; i--){
                    gmApi.renderer.bindTexture(elem.textures[i]);	
                }
                
            }else{
                gmApi.renderer.bindTexture(elem.texture);
            }
        });

        _.each(this._assetList.misc, function(elem){
            if (elem.name.indexOf("_image") > -1){
                gmApi.renderer.bindTexture(elem.texture);
            }
        });
        
    },*/

    getAsset (name:any, animated:any = true){
        var item 		= this._getItem(name);
			var	objConfig:Object|null 	= null;
			
			if (!item) {
				console.warn("Asset: "+name+" isn't loaded.");
				return null;
			}

			if( _.isObject( animated) ){
				objConfig 	= animated;
				animated 	= false;
			}else{
				animated 	= (animated != false);
			}

			var isMovieClip = (item.textures && animated);
			var asset 		= null;
			
			//Chequeo primero si es un svg luego si es un movieclip y sino es una imagen
			if ( isMovieClip)
			{
				asset 	= new PIXI.extras.AnimatedSprite( item.textures );
			}else if (item.hasOwnProperty("texture")) {
				asset	= new PIXI.Sprite( item.texture );

			}else if(item.hasOwnProperty("spineData")){
				asset   = new PIXI.spine.Spine(item.spineData);
			}
			else {
				asset   = item.data;
			}

			asset.name 	= name;
			asset.id = name;
			asset.src = item.url;

			return asset;
    }
    
    getTexture(name:any, animated:any){
        animated 		= (animated != false);
        var item 		= this._getItem(name);
        
        if (!item) {
            console.warn("Asset: "+name+" isn't loaded.");
            return null;
        }

        if (item.textures == undefined || !animated ) {
            return item.texture;
        } else {
            return item.textures;
        }
    }

    getSpriteFromAnimation (name:any, frame:any){

        var item = this._getItem(name);

        if (!item || item.textures.length < (frame - 1)) {
            console.warn("getSpriteFromAnimation: " + name + " does not exist or invalid frame.");
            return null;
        }

        var sprite	= new PIXI.Sprite(item.textures[frame]);

        return sprite;
    }

    getBackgroundMusic(moduleName:any){
        var ret = _.find(this._assetList.music, function(elem){ 
            return (elem.type == 3) && (elem.metadata.module == moduleName); 
        });

        return (ret != null) ? ret.name : "";
    }

    getAssetsByModule(moduleName:any){
        var ret = _.filter(this._assetList.image, function(elem){
            return (elem.metadata.module == moduleName);
        });

        return ret;
    }

    // Este metodo devuelve un asset del pool de assets. 
    // IMPORTANTE!!: Al dejar de usar este asset, se debe ejecutar un releaseAsset para ponerlo disponible, 
    // sino quedara alojado en memoria y no se volverá a entregar.
    getPooledAsset (name:any, animated:any){
        var item 		= this._getItem(name);
        if (!item) {
            console.warn("Asset: "+name+" isn't loaded.");
            return null;
        }
        animated 		= (animated != false);
        var isMovieClip = (item.textures && animated);
        var pool:any 		= (isMovieClip) ? this._animatedAssetsPool : this._staticAssetsPool;
        var asset:any 		= null;

        // Busco una copia disponible en el pool
        for (var i = pool.length - 1; i >= 0; i--) {
            if (!pool[i].captured && name == pool[i].name) {
                pool[i].captured	= true;
                pool[i].visible 	= true;
                //console.log("getPooledAsset - animated.length: " +_animatedAssetsPool.length + " - static.length: "+_staticAssetsPool.length);
                return pool[i];
            }
        };

        // Si no encontre una copia disponible en el pool, genero una copia nueva.
        if ( isMovieClip)
        {
            asset 	= new PIXI.extras.AnimatedSprite( item.textures );
        }else{
            asset	= new PIXI.Sprite( item.texture );
        }
        asset.name 		= name;
        asset.captured	= true;
        pool.push(asset);
        //console.log("ADD Asset to pool - animated.length: " +_animatedAssetsPool.length + " - static.length: "+_staticAssetsPool.length);
        return asset;
    }

    releaseAsset (asset:any){
        if (asset){
            asset.visible 	= false;
            asset.captured	= false;
            //if ( asset.constructor == LeanderMob.MovieClip || asset.constructor == LeanderMob.Sprite )
            //	asset.destroy();
            
        }
    }
};

