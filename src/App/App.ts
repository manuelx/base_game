import * as PIXI from 'pixi.js';
import AssetManager from '@App/AssetManager';
import Game from '@Game/Game'


export default class App extends PIXI.Application{
    
    _game: Game|null = null;
    
    constructor(options?: PIXI.ApplicationOptions | undefined){
        super(options);
        var that = this;
        
        // Carga de assets
        AssetManager.assetManager = new AssetManager();
        AssetManager.assetManager.onModuleLoaded.add((moduleName)=>{
            that.onAssetsLoaded(moduleName)
        });
        AssetManager.assetManager.start();

        // Inicio el tamaño del juego
        this.onBrowserResize();

        // Manejo de resize del browser
        window.onresize = ()=>{
            that.onBrowserResize();
        } 

    }

    // Lo llamo cuando termino de cargar cada modulo
    onAssetsLoaded(moduleName:string){
        switch(moduleName){
            case "main":
                this.onMainLoaded();
            break;
            default:
                console.log("modulo: "+moduleName+" desconocido")
        }
    }
    
    // Lo llamo cuando termino de cargar la main y puedo mostrar el juego.
    onMainLoaded(){
        console.log("Main loaded... Go Go Go");
        this._game = new Game();
        this.stage.addChild(this._game);
    }

    // Resizeo el canvas al tamaño del browser
    onBrowserResize(){
        var that = this;
    
        const browserHeight = window.innerHeight;
        const browserWidth = window.innerWidth;

        this.renderer.resize(browserWidth, browserHeight);
    
        window.scrollTo(0, 1);
    }
}